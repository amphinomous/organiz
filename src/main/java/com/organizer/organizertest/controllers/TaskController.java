package com.organizer.organizertest.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Класс отображения списка задач
 */

@Controller
public class TaskController {

    @GetMapping("/")
    public String indexPage() {
        return "index";
    }

    @GetMapping("task")
    public String taskPage() {
        return "task";
    }
}
