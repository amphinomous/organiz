package com.organizer.organizertest.model.repo;

import com.organizer.organizertest.model.Task;
import org.springframework.data.repository.CrudRepository;

public interface TaskRepository extends CrudRepository<Task, Long> {

}
